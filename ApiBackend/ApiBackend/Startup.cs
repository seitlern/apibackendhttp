﻿using System.IO;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.PlatformAbstractions;
using Swashbuckle.AspNetCore.Swagger;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace ApiBackend
{
    //Test
    public class Startup
    {
        private IHostingEnvironment _env;
        /// <summary>
        /// default constructor
        /// </summary>
        /// <param name="env"></param>
        public Startup(IHostingEnvironment env)
        {
            _env = env;
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddApiVersioning();

            var filePath = Path.Combine(PlatformServices.Default.Application.ApplicationBasePath,
                "ApiBackend.xml");
            if (File.Exists(filePath))
            {
                services.AddSwaggerGen(option =>
                {
                    AddDocInfos(option, "ApiBackend");
                    option.IncludeXmlComments(filePath);
                });
            }
            else
            {
                //documentation and reaction on 201 results on Posts are missing!!!
                services.AddSwaggerGen(option =>
                {
                    AddDocInfos(option, "ApiBackend");
                });
            }

            services.AddMvc();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseSwagger(o =>
            {
                o.RouteTemplate = "docs/api/{documentName}/swagger.json";

            });
            app.UseSwaggerUI(c =>
            {
                c.RoutePrefix = "docs-api";

                c.SwaggerEndpoint(
                    !env.IsDevelopment()
                        ? "/docs/api/ApiBackend/swagger.json"
                        : "/docs/api/ApiBackend/swagger.json",
                    "ApiBackend   V1");

                c.DocExpansion("none");
                c.SupportedSubmitMethods(new[] { "get", "post", "put", "delete" });
                c.ShowRequestHeaders();
            });

            app.UseMvc();
        }

        private void AddDocInfos(SwaggerGenOptions option, string title)
        {
            option.SwaggerDoc("ApiBackend", new Info
            {
                Version = "v1",
                Title = title,
                Description = "by Niklas Seitler",
                TermsOfService =
                    "Yolo",
                Contact = new Contact() { Name = "CGAme", Email = "yy@email.com", Url = "yolo.com" }
            });
        }
    }
}
